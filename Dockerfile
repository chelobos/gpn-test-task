#FROM centos/go-toolset-7-centos7:latest - latest, хотя позже ставим нужную версию. Лучше сразу профильное изображение использовать
FROM golang:1.14-alpine as build

ENV GO111MODULE=on
RUN apk update
RUN apk add --no-cache git bash sudo
#RUN wget https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz
#RUN tar -C /usr/local -xvzf go1.12.7.linux-amd64.tar.gz
#ENV PATH=$PATH:/usr/local/go/bin - нежелательно ставить пакеты таким образом, небезопасно+неправильно
RUN mkdir -p /app
WORKDIR /app/
COPY . .
RUN chmod +x ./src/prepare.sh && ./src/prepare.sh
RUN go mod download && go mod verify
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o server -ldflags="-w -s" .
#WORKDIR /app повтор

#Сборка из двух стадий для уменьшения размера изображения(c 300+MB до 12MB)
FROM alpine:3.16.2

EXPOSE 3000
RUN mkdir -p /app
COPY --from=build /app/ /app/
CMD ['10 15 minutes, 5 10 fifth, 4 5 10 fifth']
ENTRYPOINT /app/server
