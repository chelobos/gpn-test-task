# Тестовое задание на GPN-CUP 2022
## Инструкция по сборке и запуску приложения
### Подготовка + сборка
Все действия производить в директории с клонированным рипозиторием!
```
git clone https://gitlab.com/chelobos/gpn-test-task.git 
cd gpn-test-task
TAG=$(git rev-parse --short HEAD)
docker build . -t ${DOCKER_REGISTRY}/gpn-test-task:${TAG}
```
### Отправка в регистри
```
docker login -u ${DOCKER_USER} -p ${DOCKER_PASSWORD} ${DOCKER_REGISTRY}
docker push ${DOCKER_REGISTRY}/gpn-test-task:${TAG}
```
### Запуск
```
docker run -dit --rm -p ${PORT_OUTSIDE}:3000 ${DOCKER_REGISTRY}/gpn-test-task:${TAG}
```
